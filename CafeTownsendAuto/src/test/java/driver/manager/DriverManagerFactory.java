package driver.manager;

public class DriverManagerFactory {

    public static DriverManager  getManager(String type){

    DriverManager driverManager=null;

    if (type.equals("firefox")) {
        driverManager = new FirefoxDriverManager();
    }else if (type.equals("chrome")) {
        driverManager = new ChromeDriverManager();
    }else{
        System.out.println("Navegador incorrecto");
    }
    return driverManager;

    }
}

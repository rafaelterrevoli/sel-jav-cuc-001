package driver.manager;


import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverManager extends DriverManager {
    @Override
    public void createDriver() {

        System.setProperty("webdriver.gecko.driver",  "./driver/geckodriver.exe");
        driver = new FirefoxDriver();
    }
}

Feature: Pruebas del sitio CafeTowsend

  @prueba
  Scenario: Validar que el usuario pueda hacer login y se despliegue el home
  Given Me encuentro en el Login
  When Ingreso usuario y clave "datos_cafe"
  And Presiono boton Login
  Then Me encuentro en el home

  @prueba
  Scenario: Validar que el usuario pueda hacer logout y se despliegue la vista del login
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Presiono logout
    Then Me encuentro en el Login

  @prueba
  Scenario: Validar el nombre del usuario en el home
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Valido el nombre del usuario
    And Presiono logout
    Then Me encuentro en el Login


  @prueba
  Scenario: Validar que se pueda crear un empleado
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Presiono boton create
    And Ingreso el first name
    And Ingreso el last name
    And Ingreso el correo
    And Presiono boton save
    And Valido el empleado
    And Presiono logout
    Then Me encuentro en el Login

  @prueba
  Scenario Outline: Validar que se pueda modificar(Edit) un empleado
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Hago clic en el empleado <nombre> <apellido>
    And Presiono boton Edit
    And Ingreso el first name
    And Ingreso el last name
    And Ingreso la fecha de inicio
    And Presiono boton save
    And Valido el empleado
    And Presiono logout
    Then Me encuentro en el Login
    Examples:
      | nombre  | apellido |
      | Misko   | Hevery   |

  @prueba
  Scenario Outline: Validar que al presionar el botón <<Cancel en la vista de editar, regrese al home
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Hago clic en el empleado <nombre> <apellido>
    And Presiono boton Edit
    And Presiono <<Cancel
    And Me encuentro en el home
    And Presiono logout
    Then Me encuentro en el Login
    Examples:
      | nombre  | apellido |
      | Dan     | Doyan   |

  @prueba
  Scenario Outline: Validar que se pueda eliminar un empleado
    Given Me encuentro en el Login
    When Ingreso usuario y clave "datos_cafe"
    And Presiono boton Login
    And Me encuentro en el home
    And Hago clic en el empleado <nombre> <apellido>
    And Presiono el boton delete
    And Valido que fue eliminado <nombre> <apellido>
    And Presiono logout
    Then Me encuentro en el Login
    Examples:
      | nombre  | apellido |
      | Igor    | Minar    |
package steps;

import org.openqa.selenium.WebDriver;
import pages.CreateEmpleadoPage;
import pages.HomePage;
import pages.LoginPage;

import util.ScreenShot;

public class BaseTest {
    private WebDriver driver = Hooks.getDriver();
    protected ScreenShot screenShot = new ScreenShot(driver);
    protected LoginPage loginPage = new LoginPage(driver);
    protected HomePage homePage = new HomePage(driver);
    protected CreateEmpleadoPage createEmpleadoPage = new CreateEmpleadoPage(driver);

}

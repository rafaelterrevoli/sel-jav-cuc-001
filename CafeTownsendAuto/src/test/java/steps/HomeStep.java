package steps;

import cucumber.api.java.en.And;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static util.JsonReader.getDatos;


public class HomeStep extends BaseTest{

    @And("^Me encuentro en el home$")
    public void meEncuentroEnElHome() throws Throwable {
        try {
            assertTrue(homePage.validarHome());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar validar el home");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Presiono logout$")
    public void presionoLogout() throws Throwable {
        try {
            assertTrue(homePage.hacerClicLogout());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en Logout");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Valido el nombre del usuario$")
    public void validoElNombreDelUsuario() throws Throwable {
        String nombreUsuario = getDatos("datos_cafe").get("user");
        try {
            assertEquals(homePage.validarNombreUsuario(), nombreUsuario);
        } catch(AssertionError e) {
            throw new Exception("El nombre no coincide con el logeado");
        }finally {
            screenShot.takeScreenShot();
        }
    }


    @And("^Presiono boton create$")
    public void presionoBotonCreate() throws Throwable {
        try {
            assertTrue(homePage.hacerClicCreate());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón create");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Valido el empleado$")
    public void validoElEmpleado() throws Throwable {
        String nombre = getDatos("datos_cafe").get("nombre");
        String apellido = getDatos("datos_cafe").get("apellido");
        String nombreApellido = nombre+" "+apellido;
        try {
            assertTrue(homePage.validarQueExiteElEmpleado(nombreApellido));
        } catch(AssertionError e) {
            throw new Exception("Falló al validae el empleado ");
        }finally {
            screenShot.takeScreenShot();
        }
    }


    @And("^Hago clic en el empleado (.+) (.+)$")
    public void hagoClicEnElEmpleado(String nombre, String apellido) throws Throwable {
        String nombreApellido = nombre+" "+apellido;
        try {
            assertTrue(homePage.hacerClicEmpleado(nombreApellido));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón create");
        }finally {
            screenShot.takeScreenShot();
        }

    }

    @And("^Valido que fue eliminado (.+) (.+)$")
    public void validoQueFueEliminado(String nombre, String apellido) throws Throwable {
        String nombreApellido = nombre+" "+apellido;
        System.out.println(nombreApellido);
        try {
            assertTrue(homePage.validarQueNOExiteElEmpleado(nombreApellido));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar validar que no existe el empleado");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Presiono el boton delete$")
    public void presionoElBotonDelete() throws Throwable {
        try {
            assertTrue(homePage.hacerClicDelete());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón delete");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Presiono boton Edit$")
    public void presionoBotonEdit() throws Throwable {
        try {
            assertTrue(homePage.hacerClicEdit());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón Edit");
        }finally {
            screenShot.takeScreenShot();
        }

    }




}

package steps;

import cucumber.api.java.en.And;

import static org.junit.Assert.assertTrue;
import static util.JsonReader.getDatos;

public class CreateEmpleadoStep extends BaseTest {

    @And("^Ingreso el first name$")
    public void ingresoElFirstName() throws Throwable {
        String nombre = getDatos("datos_cafe").get("nombre");
        try {
            assertTrue(createEmpleadoPage.IngresarNombre(nombre));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar ingresar el nombre");
        }finally {
            screenShot.takeScreenShot();
        }

    }

    @And("^Ingreso el last name$")
    public void ingresoElLastName() throws Throwable {
        String apellido = getDatos("datos_cafe").get("apellido");
        try {
            assertTrue(createEmpleadoPage.IngresarApellido(apellido));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar ingresar el apellido");
        }finally {
            screenShot.takeScreenShot();
        }

    }

    @And("^Ingreso el correo$")
    public void ingresoElCorreo() throws Throwable {
        String correo = getDatos("datos_cafe").get("correo");
        try {
            assertTrue(createEmpleadoPage.IngresarCorreo(correo));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar ingresar el correo");
        }finally {
            screenShot.takeScreenShot();
        }

    }

    @And("^Ingreso la fecha de inicio$")
    public void ingresoLaFechaDeInicio() throws Throwable {
        String fecha = getDatos("datos_cafe").get("fechaInicio");
        try {
            assertTrue(createEmpleadoPage.IngresarFechaInicio(fecha));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar ingresar la fecha de inicio");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Presiono boton save$")
    public void presionoBotonSave() throws Throwable {
        try {
            assertTrue(createEmpleadoPage.hacerClicSave());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón Save");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @And("^Presiono <<Cancel$")
    public void presionoCancel() throws Throwable {
        try {
            assertTrue(createEmpleadoPage.hacerClicCancel());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón <<Cancel");
        }finally {
            screenShot.takeScreenShot();
        }

    }




}

package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;
import static util.JsonReader.getDatos;

public class LoginStep extends BaseTest{


    @Given("^Ingreso usuario y clave \"([^\"]*)\"$")
    public void ingresoUsuarioYClaveSomething(String flujoPrueba) throws Throwable {
        String usuario = getDatos(flujoPrueba).get("user");
        String clave = getDatos(flujoPrueba).get("password");
        try {
            assertTrue(loginPage.hacerLogin(usuario, clave));
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar ingresar el usuario o la clave");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @When("^Presiono boton Login$")
    public void presionoBotonLogin() throws Throwable{
        try {
            assertTrue(loginPage.hacerClicLogin());
        } catch(AssertionError e) {
            throw new Exception("Falló al intentar hacer clic en botón Login");
        }finally {
            screenShot.takeScreenShot();
        }
    }

    @Given("^Me encuentro en el Login$")
    public void meEncuentroEnElLogin() throws Throwable {
            try {
                assertTrue(loginPage.validarLogin());
            } catch(AssertionError e) {
                throw new Exception("Falló el despliegue del Login");
            }finally {
                screenShot.takeScreenShot();
            }
    }





}


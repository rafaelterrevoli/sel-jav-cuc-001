package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage extends BasePage {

    @FindBy(xpath = "//*[@id='login-form']/fieldset/label[1]/input")
    WebElement boxUserName;
    @FindBy(xpath = "//*[@id='login-form']/fieldset/label[2]/input")
    WebElement boxPasswd;
    @FindBy(xpath = "//*[@id='login-form']/fieldset/button")
    WebElement btnLogin;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean hacerLogin(String usuario, String clave) {
        ingresarTexto(boxUserName, usuario);
        ingresarTexto(boxPasswd, clave);
        return true;
    }

    public boolean hacerClicLogin() {
        hacerClicElemento(btnLogin);
        return true;
    }

    public boolean validarLogin() {
        if (!waitElementoVisible(boxUserName, 15)) return false;
        if (!waitElementoVisible(boxPasswd, 15)) return false;
        if (!waitElementoVisible(btnLogin, 15)) return false;
        return true;
    }

}

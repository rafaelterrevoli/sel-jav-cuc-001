package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage{

    @FindBy(id="bAdd")
    WebElement btnCreate;
    @FindBy(id="bEdit")
    WebElement btnEdit;
    @FindBy(id="bDelete")
    WebElement btnDelete;
    @FindBy(xpath="//*[@id='logout']/p[1]")
    WebElement btnLogout;
    @FindBy(xpath="//*[@id='logout']/p[2]")
    WebElement textUsuario;


    public HomePage (WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean validarHome(){
        if (!waitElementoVisible(btnCreate,15)) return false;
        if (!waitElementoVisible(btnLogout,15)) return false;
        return true;
    }

    public boolean hacerClicLogout() {
        hacerClicElemento(btnLogout);
        return true;
    }

    public String validarNombreUsuario(){
        return textoDelElemento(textUsuario);
    }

    public boolean validarQueExiteElEmpleado(String nombreEmpleado){
        if (!waitElementoVisible(btnCreate,15)) return false;
        By byEmpleado = By.xpath("//li[contains(text(),'"+nombreEmpleado+"')]");
         WebElement elemntoEmpleado = esperarElementoVisible(byEmpleado,15);
        return elemntoEmpleado.isDisplayed();
    }

    public boolean hacerClicEmpleado(String nombreEmpleado){
        By byEmpleado = By.xpath("//li[contains(text(),'"+nombreEmpleado+"')]");
        WebElement elemntoEmpleado = esperarElementoVisible(byEmpleado,15);
        hacerClicElemento(elemntoEmpleado);
        return true;
    }


    public boolean validarQueNOExiteElEmpleado(String nombreEmpleado){
        for(int i = 1; i <= 4; i+=1) {
            By byEmpleado = By.xpath("//*[@id='employee-list']/li["+i+"]");
            WebElement elemntoEmpleado = esperarElementoVisible(byEmpleado,15);
            if (elemntoEmpleado.getText().equals(nombreEmpleado)){
                return false;
            }
        }
        return true;
    }

    public boolean hacerClicCreate() {
        hacerClicElemento(btnCreate);
        return true;
    }

    public boolean hacerClicDelete() {
        if (!waitElementoVisible(btnDelete,15)) return false;
        hacerClicElemento(btnDelete);
        return true;
    }

    public boolean hacerClicEdit() {
        if (!waitElementoVisible(btnEdit,15)) return false;
        hacerClicElemento(btnEdit);
        return true;
    }

}

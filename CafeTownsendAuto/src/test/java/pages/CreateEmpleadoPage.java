package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateEmpleadoPage extends BasePage{

    @FindBy(xpath = "//span[contains(text(),'First name:')]/following-sibling::input[1]")
    WebElement boxFirstName;
    @FindBy(xpath = "//span[contains(text(),'Last name:')]/following-sibling::input[1]")
    WebElement boxLastName;
    @FindBy(xpath = "//span[contains(text(),'e-Mail:')]/following-sibling::input[1]")
    WebElement boxEmail;
    @FindBy(xpath = "//input[@title='Please enter a date formatting MM/DD/YYYY']")
    WebElement boxStartDate;
    @FindBy(xpath = "//button[contains(text(),'Save')]")
    WebElement bntSave;
    @FindBy(xpath = "//a[@class='subButton bCancel']")
    WebElement bntCancel;


    public CreateEmpleadoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean IngresarNombre(String nombre) {
        if (!waitElementoVisible(boxFirstName, 15)) return false;
        boxFirstName.clear();
        ingresarTexto(boxFirstName,nombre);
        return true;
    }

    public boolean IngresarApellido(String apellido) {
        if (!waitElementoVisible(boxLastName, 15)) return false;
        boxLastName.clear();
        ingresarTexto(boxLastName,apellido);
        return true;
    }

    public boolean IngresarCorreo(String correo) {
        if (!waitElementoVisible(boxEmail, 15)) return false;
        boxEmail.clear();
        ingresarTexto(boxEmail,correo);
        return true;
    }

    public boolean IngresarFechaInicio(String fecha) {
        if (!waitElementoVisible(boxStartDate, 15)) return false;
        boxStartDate.clear();
        ingresarTexto(boxStartDate,fecha);
        return true;
    }

    public boolean hacerClicSave() {
        hacerClicElemento(bntSave);
        return true;
    }

    public boolean hacerClicCancel() {
        if (!waitElementoVisible(bntCancel, 15)) return false;
        hacerClicElemento(bntCancel);
        return true;
    }


}



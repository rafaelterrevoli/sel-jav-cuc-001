package util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

public class JsonReader {

    public static Object readJson() {
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new FileReader("src/test/resources/datos.json"));
            return obj;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Map<String, String> getDatos(String cli) throws IOException {
        String rut = "";
        Object ob = readJson();
        JSONObject jsonObject = (JSONObject) ob;
        JSONArray jsonArray = (JSONArray) jsonObject.get(cli);
        String stringArreglo = jsonArray.get(0).toString();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, String> map = mapper.readValue(stringArreglo, new TypeReference<Map<String,String>>(){});
        return map;
    }


}
